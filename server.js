const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')

const server = jsonServer.create()
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'))
const facturadb = JSON.parse(fs.readFileSync('./facturas.json', 'UTF-8'))
const detalledb = JSON.parse(fs.readFileSync('./detalle-factura.json', 'UTF-8'))

server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())
server.use(jsonServer.defaults());

const SECRET_KEY = '123456789'

const expiresIn = '1h'

// Create a token from a payload
function createToken(payload){
  return jwt.sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token
function verifyToken(token){
  return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

// Check if the user exists in database
function isAuthenticated({email, password}){
  return userdb.users.findIndex(user => user.email === email && user.password === password) !== -1
}

// verifica si existe el detalle
function isDetalle({idFactura}) {
  return detalledb.findIndex(detalle => detalle.id_factura === idFactura) !== -1
}

// obtinene el datalle de la factura
function obtenerdetalle({idFactura}) {
  console.log("retornando el resultado ", detalledb.find(detalle => detalle.id_factura === idFactura))
  return detalledb.find(detalle => detalle.id_factura === idFactura)
}


server.post('/auth/login', (req, res) => {
  const {email, password} = req.body
  if (isAuthenticated({email, password}) === false) {
    const status = 401
    const message = 'Incorrect email or password'
    res.status(status).json({status, message})
    return
  }
  const access_token = createToken({email, password})
  res.status(200).json({access_token})
})

server.get('/listarfacturas', (req, res) => {
  res.status(200).json(facturadb)
})

server.post('/detallefactura', (req, res) => {
  const {idFactura} = req.body
  console.log(isDetalle({idFactura}))
  if (isDetalle({idFactura}) === false) {
    const status = 401
    const message = 'id de factura incorrecto'
    res.status(status).json({status, message})
    return
  }
  res.status(200).json(obtenerdetalle({idFactura}))
})

server.listen(4100, () => {
  console.log('Run Auth API Server port: 4100')
})
