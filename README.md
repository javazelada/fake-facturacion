# JSONServer + facturacion

un rest fake para facturacion

## Instalacion

```bash
$ npm install
$ npm run start-auth
```

## Como usar facturacion

Para el listado de todos las facturas

```
GET localhost:4100/listarfacturas
```
Me devolvera los siguientes datos

```
{
  {
        "id": 1,
        "fecha": "02/08/2018",
        "nit_emisor": 9863147569,
        "razon_social": "salon de eventos",
        "nro_factura": 2365,
        "monto": 23
    },
    {
        "id": 2,
        "fecha": "03/08/2018",
        "nit_emisor": 9863247569,
        "razon_social": "almacen oruro",
        "nro_factura": 2365,
        "monto": 23
    },

    ....
}
```

Si queremos ver el detalle de una factura

```
POST localhost:4100/detallefactura
```

con los siguientes datos en el body

```
{
	"idFactura" : 3
}

```
se tendra el siguiente resultado con el formato

```
{
    "id_factura": 3,
    "detalle": [
        {
            "cantidad": 4,
            "detalle": "platos de comida",
            "subtotal": 140
        },
        {
            "cantidad": 2,
            "detalle": "refreso",
            "subtotal": 14
        },
        {
            "cantidad": 1,
            "detalle": "mesa",
            "subtotal": 25
        },
        {
            "cantidad": 8,
            "detalle": "cubiertos",
            "subtotal": 60
        }
    ]
}
```

